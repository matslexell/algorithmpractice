public class ScalaLAstOne {
//	public static void count(int array[], int max[]){
//		int col = 0;
//		while(true) {
//			while(array[col] >= max[col]){
//				col++;
//				if(col==array.length){
//					return;
//				}
//			}
//			array[col]++;
//			col=0;
//			print(array);
//			
//		}
//	}
	
	public static void count(int array[], int max[], int col){
		if(col==array.length){
			return;
		}
		if(array[col] >= max[col]){
			count(array,max,col+1);
		} else {
			array[col]++;
			col=0;
			print(array);
			count(array, max, 0);			
		}
			
		
	}
	
	static void print(int[] array){
		for (int i : array) {
			System.out.print(i + " ");
		}
		System.out.println("");
	}
	
	public static void main(String[] args) {
			count(new int[]{0,0}, new int[]{2,4},0);
	}
}

package algorithmpractice;

import datastructures.Dictionary;

public class NoSpacesBreakup {
	Dictionary dictionary = new Dictionary();
	
	public NoSpacesBreakup() {
		String string = getSeparatedString("peanutbutter");
		System.out.println(string);
	}
	
	public String getSeparatedString(String string){		
		for (int i = 1; i <= string.length(); i++) {
			String prefix = string.substring(0, i);
			System.err.println(prefix);
			if(dictionary.isWord(prefix)){
				if(i == string.length()){
					return prefix;
				}
				
				String suffix = getSeparatedString(string.substring(i,string.length()));
				if(suffix != null){
					return prefix + " " + suffix;
				}
			}
		}
		return null;
	}

	public static void main(String[] args) {
		new NoSpacesBreakup();
	}
}

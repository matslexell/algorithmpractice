package algorithmpractice;

public class QuickSort {
	public static int[] sort(int[] list){
		return sort(list, 0, list.length-1);
	}
	
	private static int[] sort(int[] list, int idxFr, int idxTo){
		if(idxTo-idxFr<1) { return list; }
		int pivIdx = rnd(idxFr,idxTo);
		int piv = list[pivIdx];
		
		list[pivIdx] = list[idxFr];
		list[idxFr] = piv;
		
		int p1 = idxFr+1,p2 = idxTo;

		while(p1<=p2){
			if(piv >= list[p1]){
				p1++;
			}else{
				int holder = list[p2];
				list[p2] = list[p1];
				list[p1] = holder;
				p2--;
			}
		}
		
		list[idxFr] = list[p1-1];
		list[p1-1] = piv;
		
		sort(list, idxFr,p2-1);
		sort(list, p1, idxTo);
		return list;
	}
	
	static int rnd(int from, int to){
		return from + (int)(Math.random()*(to-from+1));
	}
	
	
	
	public static void main(String[] args) {
		int[] list = {6,5,5,5,6};
		for (int i = 0; i < list.length; i++) {
			System.out.print(list[i] + " ");
		}
		System.out.println();
		
		sort(list);
		
		for (int i = 0; i < list.length; i++) {
			System.out.print(list[i] + " ");
		}
		
	}
}

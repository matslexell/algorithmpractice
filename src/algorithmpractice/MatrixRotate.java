package algorithmpractice;

import java.awt.Point;

public class MatrixRotate {
	public static int[][] rotate(int[][] mtx) {
		for (int i = 0; i < mtx.length/2; i++) {
			rotate(mtx, i);	
		}
		
		return mtx;
	}
	
	public static int[][] rotate(int[][] mtx, int layer) {
		int n = mtx.length - layer * 2;
		
		for (int j = 0; j < n-1; j++) {
			Point p = new Point(j, 0);
			for(int i = 0; i < 3; i++){
				Point p2 = getPoint(p, -(n-1), n);
				swap(mtx, add(p, layer), add(p2, layer));
				p = p2;
			}			
		}
		
		return mtx;
	}
	
	public static void swap(int[][] mtx, Point a, Point b) {
		int val = mtx[a.x][a.y];
		mtx[a.x][a.y] = mtx[b.x][b.y];
		mtx[b.x][b.y] = val;
	}
	
	public static Point getPoint(Point p, int add, int n){
		return getPoint(p.x, p.y, add, n);
	}
	
	public static Point getPoint(int a, int b, int add, int n){
		if(add == 0){
			return new Point(a, b);
		}
		n = n-1;
		int size = n*4;
		add = (add+size)%size;
		
		if(a==0 && b==0 || a>b){
			a += add;
			add = 0;
			if(a>n){
				b = b + a - n;
				a = n;
			}
			if(b>n){
				add = b - n;
				b = n;
			}
			return getPoint(a, b, add, n + 1);
		} else {
			a -= add;
			add = 0;
			if(a<0){
				b = b + a;
				a = 0;
			}
			if(b<0){
				add = -b;
				b = 0;
			}
			return getPoint(a, b, add, n + 1);
		}
	}
	
	public static void print(int[][] mtx){
		StringBuilder str = new StringBuilder();
		
		for (int i = 0; i < mtx.length; i++) {
			for (int j = 0; j < mtx.length; j++) {
				String s = mtx[i][j] + "   ";
				s = s.substring(0,3);
				str.append(s);
			}
			str.append("\n");
		}
		
		System.out.println(str.toString());
	}
	
	public static Point add(Point point, int add){
		return new Point(point.x+add,point.y+add);
	}
	
	public static void main(String[] args) {
//		for (int i = 0; i < 30; i++) {
//			System.out.println(getPoint(2,2,i,3));
//		}
		
		int[][] mtx = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		print(mtx);
		
		print(rotate(mtx));
		
	}
}

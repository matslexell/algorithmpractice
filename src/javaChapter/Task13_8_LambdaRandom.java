package javaChapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Task13_8_LambdaRandom {
	public final static Random RND = new Random();

	public Task13_8_LambdaRandom() {
		List<Integer> list = new ArrayList<>();
		for (int i = 1; i <= 3; i++) {
			list.add(i);
		}

		for (int i = 0; i < 16; i++) {
			System.out.println(getRandomSubset(list));
		}

	}

	public List<Integer> getRandomSubset(List<Integer> list) {
		return list.stream().filter(p -> RND.nextBoolean())
				.collect(Collectors.toList());

	}

	public static void main(String[] args) {
		new Task13_8_LambdaRandom();
	}
}

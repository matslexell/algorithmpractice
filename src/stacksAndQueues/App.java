package stacksAndQueues;

import junit.framework.TestCase;

import org.junit.Test;

public class App extends TestCase {

	@Test
	public void testMinStack() {
		MinStack<Integer> stack = new MinStack<Integer>();
		assertTrue(stack.isEmpty());
		stack.push(5);
		assertTrue(5 == stack.peek());
		assertFalse(stack.isEmpty());
		stack.push(10);
		stack.push(12);

		assertTrue(12 == stack.peek());
		assertTrue(12 == stack.pop());
		assertTrue(10 == stack.pop());
		assertTrue(5 == stack.pop());
		assertTrue(stack.isEmpty());

		stack.push(5);
		stack.push(5);

		stack.push(7);
		stack.push(9);
		stack.push(11);
		stack.push(4);
		stack.push(4);

		stack.push(8);
		stack.push(3);
		stack.push(1);

		assertTrue(1 == stack.min());
		assertTrue(1 == stack.pop());
		assertTrue(3 == stack.min());
		assertTrue(3 == stack.pop());
		assertTrue(4 == stack.min());
		assertTrue(8 == stack.pop());
		assertTrue(4 == stack.min());
		assertTrue(4 == stack.pop());

		assertTrue(4 == stack.min());
		assertTrue(4 == stack.pop());

		assertTrue(5 == stack.min());
		assertTrue(11 == stack.pop());
		assertTrue(5 == stack.min());
		assertTrue(9 == stack.pop());
		assertTrue(5 == stack.min());
		assertTrue(7 == stack.pop());

		MinStack<Integer> stack2 = new MinStack<Integer>();

		boolean exception = false;
		try {
			stack2.pop();
		} catch (Exception e) {
			exception = true;
		}
		
		assertTrue(exception);

	}

	@Test
	public void testNormalStack() {
		Stack<Integer> stack = new NormalStack<Integer>();
		assertTrue(stack.isEmpty());
		stack.push(5);
		assertTrue(5 == stack.peek());
		assertFalse(stack.isEmpty());
		stack.push(10);
		stack.push(12);

		assertTrue(12 == stack.peek());
		assertTrue(12 == stack.pop());
		assertTrue(10 == stack.pop());
		assertTrue(5 == stack.pop());
		assertTrue(stack.isEmpty());

	}

	public static void main(String[] args) {
	}
}

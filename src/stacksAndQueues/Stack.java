package stacksAndQueues;

public interface Stack<T> {

	public T pop();
	public void push(T item);
	public T peek();
	public boolean isEmpty();
	
}

package stacksAndQueues;

public class NormalStack<T> implements Stack<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private class Node {
		Node prev;
		T data;

		Node(T data, Node prev) {
			this.data = data;
			this.prev = prev;
		}
	}

	private Node top;

	@Override
	public void push(T item) {
		Node node = new Node(item,top);
		top = node;
	}

	@Override
	public T peek() {
		if(isEmpty()){throw new RuntimeException("List empty you fool");}
		
		return top.data;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return top == null;
	}

	@Override
	public T pop() {
		if(isEmpty()){throw new RuntimeException("List empty you fool");}
		T data = top.data;
		top = top.prev;
		
		return data;
	}
}

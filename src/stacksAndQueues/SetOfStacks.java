package stacksAndQueues;

import java.util.ArrayList;
import java.util.List;

public class SetOfStacks<T> implements Stack<T> {
	final int CAPACITY = 4;

	class StackNode<E> extends NormalStack<E> {
		int size;

		@Override
		public void push(E item) {
			super.push(item);
			size++;
		}

		boolean isFilled() {
			return size == CAPACITY;
		}

		@Override
		public E pop() {
			E e = super.pop();
			size--;
			return e;
		}

	}

	List<StackNode<T>> stacks = new ArrayList<>();

	@Override
	public T pop() {
		if (isEmpty()) {
			throw new RuntimeException("List empty you fool");
		}
				
		T t = getTopStack().pop();
		
		if(getTopStack().isEmpty()){
			stacks.remove(stacks.size()-1);
		}
		
		return t;
	}

	@Override
	public void push(T item) {
		if (isEmpty() || getTopStack().isFilled()) {
			stacks.add(new StackNode<T>());
		}

		getTopStack().push(item);

	}

	@Override
	public T peek() {
		if (isEmpty()) {
			throw new RuntimeException("List empty you fool");
		}

		return getTopStack().peek();
	}

	public T popAt(int idx) {
		if (idx < 0 || idx > stacks.size() - 1) {
			throw new RuntimeException("Index out of bounds you fool");
		}

		T t = stacks.get(idx).pop();
		if (stacks.get(idx).isEmpty()) {
			stacks.remove(idx);
		}

		return t;
	}

	private StackNode<T> getTopStack() {
		return stacks.get(stacks.size() - 1);
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return stacks.isEmpty();
	}

}

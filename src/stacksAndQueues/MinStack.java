package stacksAndQueues;

public class MinStack<T extends Comparable<T>> implements Stack<T> {

	private class Node {
		Node prev;
		T data;
		Node currentMin;

		Node(T data, Node prev) {
			this.prev = prev;
			this.data = data;
		}
	}

	private Node top;

	@Override
	public T pop() {
		if (isEmpty()) {
			throw new RuntimeException("Stack is empty you fool");
		}

		T data = top.data;
		top = top.prev;

		return data;
	}

	@Override
	public void push(T item) {
		Node node = new Node(item,top);
		if(isEmpty()){
			node.currentMin = node;
		}else if (top.currentMin.data.compareTo(item) > 0){
			node.currentMin = node;
		}else {
			node.currentMin = top.currentMin;
		}
		
		top = node;
	}

	public T min() {
		if (isEmpty()) {
			throw new RuntimeException("Stack is empty you fool");
		}
		return top.currentMin.data;
	}

	@Override
	public T peek() {
		if (isEmpty()) {
			throw new RuntimeException("Stack is empty you fool");
		}
		return top.data;
	}

	@Override
	public boolean isEmpty() {
		return top == null;
	}

}

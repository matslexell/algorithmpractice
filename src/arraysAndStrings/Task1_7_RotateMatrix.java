package arraysAndStrings;

/*
 * file:///D:/Torrent/Google/Cracking%20the%20Coding%20Interview,%206th%20Edition%20189%20Programming%20Questions%20and%20Solutions.pdf#page=103
 */

public class Task1_7_RotateMatrix {

	public static void rotateRec(int[][] m, int l, int N) {
		if (N < 2) {
			return;
		}
		int n = N - 1;
		for (int i = l; i < n; i++) {
			int temp = m[l][i];
			m[l][i] = m[n - i][l];
			m[n - i][l] = m[n][n - i];
			m[n][n - i] = m[i][n];
			m[i][n] = temp;
		}
		rotateRec(m, l + 1, N - 2);
	}

	public static void rotateDyn(int[][] m) {
		int temp;
		for (int l = 0, n = m.length-1; l < m.length/2; l++, n--) {
			for (int i = 0; i < n-l; i++) {
				temp = m[l][l+i];
				m[l][i] = m[n-i][l];
				m[n - i][l] = m[n][n - i];
				m[n][n - i] = m[l+i][n];
				m[l+i][n] = temp;
			}
		}
	}

	public static void rotate(int[][] m) {
		rotateRec(m, 0, m.length);
	}

	public static void print(int[][] m) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m.length; j++) {
				String s = m[i][j] + "  ";
				str.append(s.subSequence(0, 3));
			}
			str.append("\n");
		}
		System.out.println(str);
	}

	public static void main(String[] args) {
		int[][] m1 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		int[][] m2 = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 },
				{ 13, 14, 15, 16 } };
		print(m2);
		print(m1);
		System.out.println();
		System.out.println();
		rotateDyn(m1);
		rotateDyn(m2);
		print(m2);
		print(m1);
	}
}

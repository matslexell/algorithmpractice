package arraysAndStrings;

import org.junit.Test;

import junit.framework.TestCase;

public class OneAway extends TestCase {
	public OneAway() {

	}
	
	boolean oneAway(String s1, String s2){
		return nAway(1,s1,s2);
	}
	
	boolean nAway(int edits,String s1, String s2){
		// This will cover the case if edits is -1 as well
		if(Math.abs(s1.length() - s2.length()) > edits){
			return false;
		}
		
		for (int i = 0; i < Math.min(s1.length(), s2.length()); i++) {
			if(s1.charAt(i) != s2.charAt(i)){
				if(nAway(edits-1,s1.substring(i+1),s2.substring(i+1))){
					return true;
				}
				if(nAway(edits-1,s1.substring(i+1),s2.substring(i))){
					return true;
				}
				if(nAway(edits-1,s1.substring(i),s2.substring(i+1))){
					return true;
				}
				return false;
			}
		}

		return true;
	}
	
	@Test
	public void test(){
		assertTrue(oneAway("pale","ple"));
		assertTrue(oneAway("pales","pale"));	
		assertTrue(oneAway("pale","bale"));	
		assertTrue(oneAway("very long string","very log string"));	
		assertTrue(oneAway("very long string","vury long string"));	
		assertTrue(oneAway("palea","palee"));

		assertFalse(oneAway("very long string","vury lng string"));	
		assertFalse(oneAway("pale","bake"));
		assertFalse(oneAway("1","123"));


		}

	public static void main(String[] args) {
		new OneAway();
	}
}

package coursera;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CountChange {

	public int countChange(int money, List<Integer> coins) {
		if (money == 0) {
			return 0;
		}
		if (coins.isEmpty()) {
			return 0;
		}

		return loop(money / h(coins), money, h(coins), t(coins));

	}

	private int loop(int nCoin, int money, int coin, List<Integer> coins) {
		if (nCoin < 0)
			return 0;

		int rest = money - nCoin * coin;
		int reduceNCoins = loop(nCoin - 1, money, coin, coins);
		int countChange = rest == 0 ? 1 : countChange(rest, coins);
		return reduceNCoins + countChange;

	}

	private int ways(int nCoin, int money, int coin, List<Integer> coins) {
		int ways = 0;
		while (nCoin >= 0) {
			int rest = money - coin * nCoin;

			if (rest == 0) {
				ways++;
				nCoin--;
				continue;
			}

			ways += countChange(rest, coins);
			nCoin--;
		}
		return ways;
	}

	List<Integer> t(List<Integer> list) {
		List<Integer> l = new ArrayList<>(list.size() - 1);
		for (int i = 1; i < list.size(); i++) {
			l.add(list.get(i));
		}
		return l;
	}

	int h(List<Integer> list) {
		return list.get(0);
	}

	@Test
	public void testAlg() {
		assertEquals(3, countChange(4, Arrays.asList(1, 2)));
		assertEquals(1022,
				countChange(300, Arrays.asList(5, 10, 20, 50, 100, 200, 500)));
		assertEquals(1022,
				countChange(300, Arrays.asList(100, 200, 500, 5, 10, 20, 50)));
		assertEquals(0,
				countChange(301, Arrays.asList(5, 10, 20, 50, 100, 200, 500)));
		assertEquals(0,
				countChange(0, Arrays.asList(5, 10, 20, 50, 100, 200, 500)));
		assertEquals(4, countChange(4, Arrays.asList(1, 2, 3)));
		assertEquals(3, countChange(10, Arrays.asList(5, 1)));
		assertEquals(10, countChange(10, Arrays.asList(5, 2, 1)));
		assertEquals(7, countChange(10, Arrays.asList(5, 3, 1)));
		assertEquals(20, countChange(20, Arrays.asList(5, 3, 1)));
		assertEquals(9, countChange(20, Arrays.asList(5, 8, 1)));
		assertEquals(132, countChange(100, Arrays.asList(5, 10, 15, 20, 50)));
		assertEquals(1061, countChange(200, Arrays.asList(5, 10, 15, 20, 50)));
		assertEquals(1, countChange(200, Arrays.asList(5)));
		assertEquals(41, countChange(200, Arrays.asList(5, 1)));
		assertEquals(41, countChange(200, Arrays.asList(1, 5)));
		assertEquals(0, countChange(200, Arrays.asList()));

	}

}

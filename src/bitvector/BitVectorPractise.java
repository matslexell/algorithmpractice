package bitvector;

public class BitVectorPractise {
	public BitVectorPractise() {

		int bits = 1 << 31;
		int mask = 2;
		
//		bits --;
		String substring = "123456789".substring(1);
		System.out.println(substring);
//		bits = bits << 1;
		
		System.out.println(Integer.toBinaryString(bits));
		System.out.println(bits);
	}
	
	public int toggle(int bits, int index){
		if((bits & index) == 0){
			bits |= index;
		}else {
			bits &= ~index;
		}
		return bits;
	}
	
	
	public static void main(String[] args) {
		new BitVectorPractise();
	}
}

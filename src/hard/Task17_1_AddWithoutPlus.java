package hard;

public class Task17_1_AddWithoutPlus {
	public static int add(int n, int mask) {
		do {
			int temp = n ^ mask;
			mask = n & mask;
			mask=mask<<1;
			n = temp;
		} while (mask != 0);
		return n;
	}
	
	public static void main(String[] args) {
		System.out.println(add(10,15));
	}

}

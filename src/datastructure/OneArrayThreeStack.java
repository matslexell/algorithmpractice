package datastructure;

public class OneArrayThreeStack {
	int oneArray[] = new int[200];
	
	public OneArrayThreeStack(){
		
	}

	public int popOne() {
		int size = oneArray[0];
		if(size <= 0){
			throw new RuntimeException("Index out of bounds");
		}
		/*
		 * List one starts at position three, next element is at position 6,
		 * next at pos 9. So, biggest element will be at pos size*3.
		 */
		
		//Decrease size when popping!
		oneArray[0] = size - 1;

		return oneArray[size*3];
	}

	public void addOne(int i) {
		
	}

	public void addTwo() {
		int size = oneArray[1];

	}

	public void popTwo(int i) {

	}

	public int popThree() {
		int size = oneArray[2];
		return 0;

	}

	public void addThree(int i) {

	}

	public static void main(String[] args) {

	}
}

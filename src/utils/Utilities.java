package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Utilities {
	
	public static void writeToTopOfFile(String string, String path){
		String content = readFromFile(path);
		writeToFile(string + "\n" + content, path, false);
	}
	
	public static String readFromFile(String path) {



        File file = new File(path);
        Scanner s;
        String content = "";
        try {
            s = new Scanner(file);
            content = s.useDelimiter("\\Z").next();
            s.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace(); // See comment about logging framework
        } catch (NoSuchElementException e){
        	content = "";
        }
        return content;
    }
	
	public static void writeToFile(String string, String path, boolean write){

		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path, write)))) {
		    out.print(string);
		}catch (IOException e) {
		    e.printStackTrace();
		}
	}
	
	public static void writeToFile(String string, String path){
		writeToFile(string,path,true);
	}
	
	public static <T> String toString(T t[]){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < t.length; i++) {
			sb.append(t[i].toString() + " ");
		}
		return sb.toString();
	}
	
	public static <T> String toString(T t[][]){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < t.length; i++) {
			sb.append(toString(t[i]) + "\n");

		}
		return sb.toString();
	}
	
	public static String toString(int[] array){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			sb.append(array[i] + " ");
		}
		
		return sb.toString();
	}
	
	public static String toString(int[][] array){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			sb.append(toString(array[i]) + "\n");
		}
		return sb.toString();
	}
	
	public static int[] copy(int[] array){
		int[] arrayCopy = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			arrayCopy[i] = array[i];
		}
		
		return arrayCopy;
	}
	
	public static void main(String[] args) {
		Utilities.writeToFile("Mats", "D:\\git\\Mats\\backupapplication\\dummy");
		
		String[] i = new String[10];
		
		for (int j = 0; j < i.length; j++) {
			i[j] = j + "";
		}
		
		System.err.println(toString(i));
		
	}
}

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import utils.Utilities;

public class GiveTask {

	String[] tasksDone = { "1.7", "3.1", "4.1", "4.2", "4.3", "4.4", "4.5",
			"13.8" };

	int[] nTasks = { 9, 8, 6, 12, 8, 10, 12, 14, 8, 11, 6, 11, 8, 7, 7, 26, 26 };
	int[] taskPages = { 90, 94, 98, 109, 115, 122, 127, 134, 144, 149, 157,
			163, 167, 172, 179, 181, 186 };
	int[] chapterPages = { 88, 92, 96, 100, 112, 117, 125, 130, 137, 146, 152,
			158, 165, 169, 174, 181, 186 };
	int[] solutionPages = { 192, 208, 227, 241, 276, 289, 305, 342, 372, 396,
			417, 422, 433, 441, 447, 462, 530 };
	String[] chapters = { "Arrays and Strings", "Linked Lists",
			"Stacks And Queues", "Trees And Graphs", "Bit Manipulation",
			"Math and Logic Puzzles", "Object-Oriented Design",
			"Recursion and Dynamic Programming", "System Design",
			"Sorting & Searching", "Testing", "C and C++", "Java", "Databases",
			"Threads and Locks", "Moderate", "Hard" };

	public GiveTask() {

	}

	public void giveTasks() {
		StringBuilder str = new StringBuilder();
		Date date = new Date(System.currentTimeMillis());
		str.append(new SimpleDateFormat("HH:mm:ss (yyyy-MM-dd)").format(date));
		str.append(" ---------------------- \n\n");
		str.append(giveRandomTask() + "\n");
		str.append(giveRandomTask() + "\n");
		str.append(giveRandomTask() + "\n");
		str.append(giveRandomTask() + "\n");
		str.append("\n");
		System.out.println(str.toString());
		Utilities.writeToTopOfFile(str.toString(), "log.txt");

	}

	public String giveRandomTask() {
		StringBuilder str = new StringBuilder();

		int chapter = randomChapter();
		String task = (chapter + 1) + "." + randomTask(chapter);
		if (taskDone(task)) {
			return giveRandomTask();
		}

		str.append("Task: " + task + "\n");
		str.append("Page task: " + taskPages[chapter] + " (or "
				+ (taskPages[chapter] + 12) + ")\n");
		str.append("Page chapter: " + chapterPages[chapter] + " (or "
				+ (chapterPages[chapter] + 12) + ")\n");
		str.append("Chapter: " + chapters[chapter] + "\n");
		str.append(linksTo(chapter));

		// java.awt.Desktop.getDesktop().browse(java.net.URI.create(link(pages[chapter])));
		return str.toString();
	}

	public boolean taskDone(String task) {
		for (String taskDone : tasksDone) {
			if (task.equals(taskDone)) {
				return true;
			}
		}
		return false;
	}

	public List<String> getAllTasks() {
		ArrayList<String> list = new ArrayList<>();

		for (int i = 0; i < nTasks.length; i++) {
			for (int j = 0; j < nTasks[i]; j++) {
				list.add((i + 1) + "." + (j + 1));
			}
		}
		return list;
	}

	public int randomChapter() {
		return (int) (Math.random() * nTasks.length);
	}

	public int randomTask(int chapter) {
		return 1 + (int) (Math.random() * nTasks[chapter]);
	}

	public String link(int page) {
		return "file:///D:/Torrent/Google/Cracking%20the%20Coding%20Interview,%206th%20Edition%20189%20Programming%20Questions%20and%20Solutions.pdf#page="
				+ (page + 12);
	}
	
	public  String linksTo(String chapter){
		for (int i = 0; i < chapters.length; i++) {
			if(chapters[i].equals(chapter)){
				return linksTo(i);
			}
		}
		return "";
	}
	
	public  String linksTo(int chapter){
		StringBuilder str = new StringBuilder();
		str.append("(chapter:task:solution)\n");
		str.append(link(chapterPages[chapter]) + "\n");
		str.append(link(taskPages[chapter]) + "\n");
		str.append(link(solutionPages[chapter]) + "\n");
		return str.toString();
	}

	public static void main(String[] args) {
		new GiveTask().giveTasks();;
//		System.out.println(new GiveTask().linksTo(0));

	}
}
// 12 more
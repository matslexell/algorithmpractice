package datastructures;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Dictionary {
	private String[] words = { "my", "dog", "ate", "my", "homework", "do" };
	private List<String> list = Arrays.asList(words);

	public Dictionary() {
		list = loadDictionary();
	}

	public boolean isWord(String word) {
		return index(word) != -1;
	}

	public int index(String word) {
		return index(0, list.size(), word);
	}

	private int index(int from, int to, String word) {
		word = word.toLowerCase();
		int index = (from + to) / 2;
		String w = list.get(index).toLowerCase();
		int comp = word.compareTo(list.get(index));

		if (comp == 0) {
			return index;
		} else if (from == to) {
			return -1;
		} else if (comp < 0) {
			return index(from, index, word);
		}
		return index(index + 1, to, word);

	}

	private List<String> loadDictionary() {
		List<String> list = Arrays.asList(readFromFile("dictionary.txt")
				.toLowerCase().replace(" ", "").replace("\t", "").split("\n"));

		Collections.sort(list);
		
		return list;
	}

	private String readFromFile(String path) {
		File file = new File(path);
		Scanner s;
		String content = "";
		try {
			s = new Scanner(file);
			content = s.useDelimiter("\\Z").next();
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace(); // See comment about logging framework
		}
		return content;
	}

	public static void main(String[] args) {
		Dictionary dic = new Dictionary();
		System.out.println(dic.index("name"));
	}
}

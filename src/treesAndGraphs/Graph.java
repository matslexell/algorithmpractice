package treesAndGraphs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Graph implements Iterable<GraphNode> {
	private Map<String, GraphNode> nodes = new HashMap<>();

	public GraphNode get(String name) {
		name = name.replace(" ", "");
		if (nodes.get(name) == null) {
			throw new RuntimeException("No node with that name");
		}

		return nodes.get(name);
	}

	public void add(String... node) {
		for (String string : node) {
			addSub(string);
		}
	}
	
	private void addSub(String node){
		if (node.contains("->")) {
			throw new RuntimeException(
					"You added a connection (->), not possible for add");
		}
		if(nodes.get(node) != null){
			throw new RuntimeException(
					"node already exists");
		}
		node = node.replace(" ", "");
		nodes.put(node, new GraphNode(node));
		
	}
	
	public void addConnection(String...connection){
		for (String string : connection) {
			addConnectionSub(string);
		}
	}

	private void addConnectionSub(String connection) {
		connection = connection.replace(" ", "");
		if (!connection.contains("->")) {
			throw new RuntimeException("You forgot the connection (->)");
		}
		;

		String node[] = connection.split("->");

		if (node.length != 2) {
			throw new RuntimeException("Something wrong with the format");
		}
		;

		GraphNode left = nodes.get(node[0]);
		GraphNode right = nodes.get(node[1]);

		if (left == null) {
			left = new GraphNode(node[0]);
			nodes.put(node[0], left);
		}

		if (right == null) {
			right = new GraphNode(node[1]);
			nodes.put(node[1], right);
		}

		left.add(right);

	}

	public String toString() {
		StringBuilder str = new StringBuilder();
		for (GraphNode graphNode : nodes.values()) {
			str.append(graphNode.toString() + "\n");
		}

		if (str.length() > 0) {
			str.delete(str.length() - 1, str.length());
		}
		return str.toString();
	}

	@Override
	public Iterator<GraphNode> iterator() {
		return nodes.values().iterator();
	}
}

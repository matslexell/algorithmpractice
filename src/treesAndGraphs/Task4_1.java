package treesAndGraphs;

import java.util.LinkedList;
import java.util.Queue;

import treesAndGraphs.GraphNode.State;

public class Task4_1 {
	public Task4_1() {
		Graph g = build1();
		
		boolean find = find(g, g.get("c"),g.get("d"));
		System.out.println(find);
	}
	
	public boolean find(Graph g, GraphNode start, GraphNode end){
		if(start == end){
			return true;
		}
		
		for (GraphNode node : g) {
			node.setState(State.Unvisited);
		}
		
		
		
		

		return bfs(start,end);
	}
	
	public boolean dfs(GraphNode start, GraphNode end){

		if(start.getState() == State.Visited){
			return false;
		}
		
		if(visit(start,end)){
			return true;
		}
		
		
		for (GraphNode node : start) {
			if(dfs(node,end)){
				return true;
			}
		}
		return false;
	}
	
	public boolean bfs(GraphNode start, GraphNode end){

		Queue<GraphNode> queue = new LinkedList<>();
		queue.add(start);
		
		while(!queue.isEmpty()){
			GraphNode visit = queue.poll();
			if(visit(visit,end)){
				return true;
			}
			for (GraphNode n : visit) {
				if(n.getState() == State.Unvisited){
					queue.add(n);
				}
			}
			
		}
		return false;
	}
	
	public boolean visit(GraphNode visited, GraphNode target){
		System.out.println("Visiting: " + visited.toString());
		visited.setState(State.Visited);
		return visited == target;
	}
	
	
	public Graph build1(){
		Graph g = new Graph();
		g.addConnection("a -> c");
		g.addConnection("a -> d");
		
		g.addConnection("b -> c");
		
		g.addConnection("c -> a");
		g.addConnection("c -> b");
		g.addConnection("c -> e");
		
		g.addConnection("d -> c");
		return g;
	}

	public static void main(String[] args) {
		new Task4_1();
	}
}

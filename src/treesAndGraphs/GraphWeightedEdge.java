package treesAndGraphs;

public class GraphWeightedEdge {
	GraphWeightedNode node;
	int weight;
	
	public GraphWeightedEdge(GraphWeightedNode node, int weight){
		this.weight = weight;
		this.node = node;
	}
	
	public GraphWeightedNode getNode(){
		return node;
	}
	public int getWeight(){
		return weight;
	}
}

package treesAndGraphs.binarySearchTrees;

import java.util.LinkedList;
import java.util.List;

import treesAndGraphs.BTUtils;

public class Task4_3 extends SBTree<Integer> {
	
	List<List<SBTNode<Integer>>> lists = new LinkedList<>();
	
	public List<List<SBTNode<Integer>>> toLists(){
		return toLists(top,new LinkedList<>(),0);
	}
	
	private List<List<SBTNode<Integer>>> toLists(SBTNode<Integer> node,
			LinkedList<List<SBTNode<Integer>>> linkedList, int level) {
		if(node == null){
			return linkedList;
		}
		
		if(linkedList.size() <= level){
			linkedList.add(new LinkedList<>());
		}
		toLists(node.left,linkedList,level+1);
		toLists(node.right,linkedList,level+1);

		linkedList.get(level).add(node);


		return linkedList;
	}

	public static void main(String[] args) {
		Task4_3 tree = new Task4_3();
		int[] array = BTUtils.getRandomList(32, 0, 99);
		
		for (int i : array) {
			tree.add(i);
		}
		
		List<List<SBTNode<Integer>>> lists = tree.toLists();
		
		for (List<SBTNode<Integer>> list : lists) {
			System.out.println(list);
		}
		
		System.out.println(tree.toString());
		
	}
	
}

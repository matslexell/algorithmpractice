package treesAndGraphs.binarySearchTrees;

class BinaryTreeBuilder {
	enum Direction {
		Right, Left;

		char direction() {
			return this == Left ? '/' : '\\';
		}

	}

	private final static char[][] TEMPLATE = {
			"                       0                       ".toCharArray(),
			"                      / \\                      ".toCharArray(),
			"                     /   \\                     ".toCharArray(),
			"                    /     \\                    ".toCharArray(),
			"                   /       \\                   ".toCharArray(),
			"                  /         \\                  ".toCharArray(),
			"                 /           \\                 ".toCharArray(),
			"                /             \\                ".toCharArray(),
			"               /               \\               ".toCharArray(),
			"              /                 \\              ".toCharArray(),
			"             /                   \\             ".toCharArray(),
			"            /                     \\            ".toCharArray(),
			"           1                       1           ".toCharArray(),
			"          / \\                     / \\          ".toCharArray(),
			"         /   \\                   /   \\         ".toCharArray(),
			"        /     \\                 /     \\        ".toCharArray(),
			"       /       \\               /       \\       ".toCharArray(),
			"      /         \\             /         \\      ".toCharArray(),
			"     2           2           2           2     ".toCharArray(),
			"    / \\         / \\         / \\         / \\    ".toCharArray(),
			"   /   \\       /   \\       /   \\       /   \\   ".toCharArray(),
			"  3     3     3     3     3     3     3     3  ".toCharArray(),
			" / \\   / \\   / \\   / \\   / \\   / \\   / \\   / \\ "
					.toCharArray(),
			"4   4 4   4 4   4 4   4 4   4 4   4 4   4 4   4".toCharArray(),
			"                                               ".toCharArray() };

	private char[][] t;

	public BinaryTreeBuilder() {
	}

	public BinaryTreeBuilder buildTree(Printable top) {
		t = copy(TEMPLATE);
		if (top == null || top.getData() == null) {
			t = new char[0][0];
			return this;
		}
		insert(t[0], 23, 0, top, Direction.Left);
		goDown(23, 0, 1, top.getLeft(), Direction.Left);
		goDown(23, 0, 1, top.getRight(), Direction.Right);
		return this;
	}

	private char[][] copy(char[][] template) {
		char[][] t = new char[template.length][];

		for (int i = 0; i < t.length; i++) {
			t[i] = template[i].clone();
		}

		return t;
	}

	private void goDown(int x, int y, int level, Printable node,
			Direction direction) {

		final char edge = direction.direction();

		do {
			if (direction == Direction.Left) {
				y++;
				x--;
			} else {
				y++;
				x++;
			}
		} while (t[y][x] == edge);

		if (node == null || node.getData() == null) {
			destroyDown(x, y, level, Direction.Left);
			destroyDown(x, y, level, Direction.Right);
			return;
		}
		insert(t[y], x, level, node, direction);
		if (level == 4) {
			if (node.getLeft() != null && node.getLeft().getData() != null) {
				t[y + 1][x] = '.';
			}
			if (node.getRight() != null && node.getRight().getData() != null) {
				t[y + 1][x] = '.';
			}
			return;
		}
		goDown(x, y, level + 1, node.getLeft(), Direction.Left);
		goDown(x, y, level + 1, node.getRight(), Direction.Right);
	}

	private void destroyDown(int x, int y, int level, Direction direction) {
		final char edge = direction.direction();
		t[y][x] = ' ';
		if (level == 4) {
			return;
		}

		while (true) {
			if (direction == Direction.Left) {
				y++;
				x--;
			} else {
				y++;
				x++;
			}
			if (t[y][x] != edge) {
				break;
			}
			t[y][x] = ' ';
		}
		destroyDown(x, y, level + 1, Direction.Left);
		destroyDown(x, y, level + 1, Direction.Right);
	}

	private void insert(char[] array, int pos, int level, Printable node,
			Direction direction) {

		int[] subStringSizes = { 47, 23, 11, 5, 2 };
		String nodeString = node.getData().toString();
		char[] string = nodeString.substring(0,
				Math.min(nodeString.length(), subStringSizes[level]))
				.toCharArray();

		int leftShift = (string.length - 1) / 2;
		if (level == 4) {
			if (direction == Direction.Right) {
				if (string.length == 2) {
					leftShift = 1;
				}
			}

		}

		for (int i = 0; i < string.length; i++) {
			array[pos - leftShift + i] = string[i];
		}

	}

	public String toString() {
		return charToString(t);
	}

	private String charToString(char[][] t) {
		StringBuilder str = new StringBuilder();

		for (int i = 0; i < t.length; i++) {
			if (rowIsEmpty(t[i])) {
				break;
			}
			for (int j = 0; j < t[i].length; j++) {
				str.append(t[i][j]);
			}
			str.append("\n");
		}
		if (str.length() > 0) {
			str.delete(str.length() - 1, str.length());
		}

		return str.toString();

	}

	private boolean rowIsEmpty(char[] cs) {
		for (char c : cs) {
			if (c != ' ') {
				return false;
			}
		}
		return true;
	}
}

package treesAndGraphs.binarySearchTrees;

public class SBTree<T extends Comparable<T>> {
	SBTNode<T> top = new SBTNode<T>();
	private int size;

	public boolean add(T... t) {
		boolean result = false;
		for (T data : t) {
			if (top.add(data)) {
				size++;
				result = true;
			}			
		}
		return result;
	}
	
	public T remove(T... t){
		T lastRemoved = null;
		for (T data : t) {
			lastRemoved = top.remove(data);
		}
		return lastRemoved;
	}

	public int size() {
		return size;
	}

	public T find(T t) {
		return top.find(t);
	}

	public String toString() {
		BinaryTreeBuilder tree = new BinaryTreeBuilder().buildTree(top);
		return tree.toString();

	}
}

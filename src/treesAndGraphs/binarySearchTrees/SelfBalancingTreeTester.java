package treesAndGraphs.binarySearchTrees;

import java.util.Arrays;

import treesAndGraphs.BTUtils;

public class SelfBalancingTreeTester {
	public SelfBalancingTreeTester() {
		test1();
	}
	
	public void test1(){
//		int[] array = BTUtils.getRandomList(31,-9,99);
		int[] array = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		int[] arrayToIdeal = {9,10,11,12,13,14,15};

		array = BTUtils.shuffle(array);
		array = BTUtils.place(array);
		arrayToIdeal = BTUtils.place(arrayToIdeal);
		
		
//		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		SBTree<Integer> tree = new SBTree<Integer>();
		SBTree<Integer> ideal = new SBTree<Integer>();

		tree.add(1,2,3,4,5,6);
		tree.add(7,9,10,11,12,13,14,15,17);
//		tree.add(12);
		
		tree.remove(6,12,14,1,4,13,17,9);
		
//		for (int i : array) {
//			tree.add(i);
//		}
		for (int i : arrayToIdeal) {
			ideal.add(i);
		}
		
		
//		System.out.println(Arrays.toString());
//		tree.remove();
//		tree.add(10);
//		tree.remove(14);
		System.out.println(tree.toString() + "\n");
//		System.out.println(ideal.toString());

		
	}

	public static void main(String[] args) {
		new SelfBalancingTreeTester();
	}
}

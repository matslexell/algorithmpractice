package treesAndGraphs.binarySearchTrees;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.script.SimpleScriptContext;

public class BinarySearchTree<T extends Comparable<T>> {

	 TreeNode<T> top = new TreeNode<T>();
	private int size;

	public boolean add(T t) {
		if (top.add(t)) {
			size++;
			return true;
		}
		return false;
	}

	public int size() {
		return size;
	}

	public T find(T t) {
		return top.find(t);
	}

	public String toString() {
		BinaryTreeBuilder tree = new BinaryTreeBuilder().buildTree(top);
		return tree.toString();

	}

	public String simpleString() {
		ArrayList<ArrayList<String>> list = new ArrayList<>(0);
		simpleString(list, top, 0);
		list.remove(list.size() - 1);
		StringBuilder str = new StringBuilder();
		for (ArrayList<String> arrayList : list) {
			str.append(arrayList.toString() + "\n");
		}

		return str.toString();
	}

	private void simpleString(ArrayList<ArrayList<String>> list, TreeNode<T> node,
			int level) {
		if (list.size() <= level) {
			list.add(new ArrayList<String>());
		}

		if (node == null || node.data == null) {
			list.get(level).add(null);
			return;
		} else {
			list.get(level).add(node.data.toString());
		}
		simpleString(list, node.left, level + 1);
		simpleString(list, node.right, level + 1);
	}

	public static void main(String[] args) {
		BinarySearchTree<Integer> t = null;

		do {
			t = new BinarySearchTree<Integer>();

			for (int i = 0; i < 31; i++) {
				int rdn = (int) (Math.random() * 99);
				t.add(rdn);
			}
			// System.out.println();
			// System.out.println(t.simpleString());
			// System.out.println();
			// System.out.println(t.toString());
			System.out.println(t.toString());
		} while (t.toString().contains("."));
		System.out.println(t.size());
	}

}

package treesAndGraphs.binarySearchTrees;

interface Printable {
	public Printable getLeft();
	public Printable getRight();
	public Object getData();
}

package treesAndGraphs.binarySearchTrees;




class TreeNode<T extends Comparable<T>> implements Printable {

	T data;
	TreeNode<T> left;
	TreeNode<T> right;

	boolean add(T t) {
		if (data == null) {
			data = t;
			return true;
		} else if (t.compareTo(data) < 0) {
			if (left == null) {
				left = new TreeNode<T>();
			}
			return left.add(t);
		} else if (t.compareTo(data) > 0) {
			if (right == null) {
				right = new TreeNode<T>();
			}
			return right.add(t);
		}
		return false;
	}

	T find(T t) {
		if (data == null) {
			return null;
		}
		if (t.compareTo(data) == 0) {
			return t;
		} else if (t.compareTo(data) < 0) {
			return left == null ? null : left.find(t);
		}
		// else if (t.compareTo(data) > 0)
		return right == null ? null : right.find(t);

	}

	int depth() {
		if(data == null){
			return 0;
		}
		return 1 + Math.max(left == null ? 0 : left.depth(), right == null ? 0
				: right.depth());
	}

	@Override
	public Printable getLeft() {
		return left;
	}

	@Override
	public Printable getRight() {
		return right;
	}

	@Override
	public Object getData() {
		return data;
	}

}

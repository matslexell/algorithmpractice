package treesAndGraphs.binarySearchTrees;

class SBTNode<T extends Comparable<T>> implements Printable {
	enum Direction {
		Right, Left
	}

	 T data;
	 SBTNode<T> left;
	 SBTNode<T> right;
	private byte w = 0;

	public boolean add(T t) {
		boolean result = false;
		if (data == null) {
			data = t;
			result = true;
		} else if (t.compareTo(data) < 0) {
			if (left == null) {
				left = new SBTNode<T>();
			}
			result = left.add(t);
			if (result) {
				w--;
			}
		} else if (t.compareTo(data) > 0) {
			if (right == null) {
				right = new SBTNode<T>();
			}
			result = right.add(t);
			if (result) {
				w++;
			}
		}
		balance();
		return result;
	}

	public T remove(T t) {
		return remove(t, null);
	}

	private T remove(T t, SBTNode<T> father) {
		if (t.compareTo(data) == 0) {
			return remove(father);
		}
		T result = null;
		if (t.compareTo(data) < 0) {
			result = left == null ? null : left.remove(t, this);
			if(result != null){
				w++;
			}
		} else /* if(t.compareTo(data) > 0) */{
			result = right == null ? null : right.remove(t, this);
			if(result != null){
				w--;
			}
		}
		balance();
		return result;
	}

	private void balance() {
		if(-2 < w && w < 2){
			return;
		}
		T data = this.data;
		if(w <= -2){
			replaceWithClosest(Direction.Left);
			left.balance();
		} else if (w >= 2){
			replaceWithClosest(Direction.Right);
			right.balance();
		}
		add(data);
	}

	private T remove(SBTNode<T> father) {
		T t = data;
		if (left == null && right == null) {
			// Remove self
			if (father == null) {
				data = null;
			} else if (this == father.left) {
				father.left = null;
			} else { // this == father.right
				father.right = null;
			}
		}

		else if (left == null) {// Right is not null
			if (father == null) {
				replaceWithClosest(Direction.Right);
			} else if (this == father.left) {
				father.left = right;
				
			} else {
				father.right = right;
			}
		}

		else if (right == null) { // Left is not null
			if (father == null) {
				replaceWithClosest(Direction.Left);
			} else if (this == father.left) {
				father.left = left;
			} else {
				father.right = left;
			}
		} else {
			replaceWithClosest(Direction.Left);
		}

		return t;
	}

	private void replaceWithClosest(Direction dir) {
		if (dir == Direction.Left) {
			w++;
			SBTNode<T> node = left;
			if (node.right == null) {
				data = node.data;
				left = node.left;
			} else {
				while (node.right.right != null) {
					node.w--;
					node = node.right;
				}
				node.w--;
				data = node.right.data;
				node.right = node.right.left;
			}
		} else if (dir == Direction.Right) {
			w--;
			SBTNode<T> node = right;
			if (node.left == null) {
				data = node.data;
				right = node.right;
			} else {
				while (node.left.left != null) {
					node.w++;
					node = node.left;
				}
				node.w++;
				data = node.left.data;
				node.left = node.left.right;
			}
		}
	}

	T find(T t) {
		if (data == null) {
			return null;
		}
		if (t.compareTo(data) == 0) {
			return t;
		} else if (t.compareTo(data) < 0) {
			return left == null ? null : left.find(t);
		}
		// else if (t.compareTo(data) > 0)
		return right == null ? null : right.find(t);

	}

	int depth() {
		if (data == null) {
			return 0;
		}
		return 1 + Math.max(left == null ? 0 : left.depth(), right == null ? 0
				: right.depth());
	}

	@Override
	public Printable getLeft() {
		return left;
	}

	@Override
	public Printable getRight() {
		return right;
	}

	@Override
	public Object getData() {
		return data;
	}
	
	public String toString() {
		return data.toString();
//		return new TreeBuilder().buildTree(this).toString();
	}

}

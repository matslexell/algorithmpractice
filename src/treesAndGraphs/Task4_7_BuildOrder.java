package treesAndGraphs;

import org.junit.Test;

public class Task4_7_BuildOrder {
	public Task4_7_BuildOrder() {
//		 (a, d), (f, b), (b, d), (f, a), (d, c) 
		Graph g = new Graph();
		g.add("a","b","c","d","e","f");
		g.addConnection("a -> d");
		g.addConnection("f -> b");
		g.addConnection("b -> d");
		g.addConnection("f -> a");
		g.addConnection("d -> c");
		
		g.addConnection("d -> a");
		g.addConnection("b -> f");
		g.addConnection("d -> b");
		g.addConnection("a -> f");
		g.addConnection("c -> d");
		
		System.out.println(g.toString());
		
	}
	
	@Test
	public void test(){
		
	}
	
	public static void main(String[] args) {
		new Task4_7_BuildOrder();
	}
}

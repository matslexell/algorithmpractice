package treesAndGraphs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GraphNode implements Iterable<GraphNode>{
	private List<GraphNode> neigbours = new ArrayList<>();
	private State state;
	private String name;
	
	public GraphNode(String name){
		this.name = name;
	}
	
	public enum State {
		Unvisited, Visited, Visiting; 
	}
	
	public State getState(){
		return state;
	}
	
	public void setState(State state){
		this.state = state;
	}
	
	public void add(GraphNode node){
		if(neigbours.contains(node)){throw new RuntimeException("Neighbours already connected: " + toString() + "->" + node.toString());}
		neigbours.add(node);
	}
	
	public int size(){
		return neigbours.size();
	}
	
	public GraphNode get(int index){
		return neigbours.get(index);
	}
	
	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append("[");
		for (GraphNode graphNode : neigbours) {
			str.append(graphNode.name + ", ");
		}
		if(!neigbours.isEmpty()){
			str.delete(str.length()-2, str.length());
		}
		str.append("]");
		
		return name + " " + str.toString();
	}
	
	@Override
	public Iterator<GraphNode> iterator() {
		return neigbours.iterator();
	}
	
}

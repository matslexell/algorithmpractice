package treesAndGraphs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GraphWeightedNode implements Iterable<GraphWeightedEdge>{
	private List<GraphWeightedEdge> neigbours = new ArrayList<>();
	private State state;
	private String name;
	
	public GraphWeightedNode(String name){
		this.name = name;
	}
	
	public enum State {
		Unvisited, Visited, Visiting; 
	}
	
	public State getState(){
		return state;
	}
	
	public void setState(State state){
		this.state = state;
	}
	
	public void add(GraphWeightedNode node, int weight){
		// if(neigbours.contains(node)){throw new RuntimeException("Neighbours already connected: " + toString() + "->" + node.toString());}
		neigbours.add(new GraphWeightedEdge(node, weight));
	}
	
	public int size(){
		return neigbours.size();
	}
	
	public GraphWeightedEdge get(int index){
		return neigbours.get(index);
	}
	
	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append("[");
		for (GraphWeightedEdge graphNode : neigbours) {
			str.append(graphNode.getNode().name + " (" + graphNode.getWeight() + "), ");
		}
		if(!neigbours.isEmpty()){
			str.delete(str.length()-2, str.length());
		}
		str.append("]");
		
		return name + str.toString();
	}
	
	@Override
	public Iterator<GraphWeightedEdge> iterator() {
		return neigbours.iterator();
	}
	
}

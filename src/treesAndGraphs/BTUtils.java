package treesAndGraphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BTUtils {
	
	public static int[] place(int[] array){
		if(array.length == 0){
			return null;
		}
		Arrays.sort(array);
		List<Integer> list = place(array,new ArrayList<Integer>(),0,array.length-1);
		for (int i = 0; i < array.length; i++) {
			array[i] = list.get(i);
		}
		
		return array;
	}
	
	
	private static List<Integer> place(int[] array,List<Integer> list, int from, int to) {
		int idx = (from+to)/2;
		list.add(array[idx]);
		if(idx != from){
			place(array,list, from, idx-1);
		}
		
		if(idx != to){
			place(array,list, idx+1, to);
		}	
		return list;
	}
	
	public static int[] getRandomList(int size, int from, int to){
		if(size > 1 + to - from){
			throw new RuntimeException("Range error");
		}
		
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < size; i++) {
			while(true){
				int rdn = from + (int) (Math.random() * (to-from+1));
				if(!list.contains(rdn)){
					list.add(rdn);
					break;
				}
			}
		}
		int[] array = new int[list.size()];
		for (int i = 0; i < array.length; i++) {
			array[i] = list.get(i);
		}
		
		return array;
	}
	
	public static int[] shuffle(int[] array){
		array = array.clone();
		for (int i = 0; i < array.length; i++) {
			int rndIdx = i + (int) (Math.random()*(array.length-i));
			// Swap
			int holder = array[rndIdx];
			array[rndIdx] = array[i];
			array[i] = holder;
		}
		return array;
	}
}

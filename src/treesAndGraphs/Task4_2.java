package treesAndGraphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import treesAndGraphs.binarySearchTrees.BinarySearchTree;
import treesAndGraphs.binarySearchTrees.SBTree;

public class Task4_2 {
	public Task4_2() {
		int[] array = BTUtils.getRandomList(31,0,30);
		System.out.println(Arrays.toString(array));
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();

//		place(array,tree);
		List<Integer> list = place(array);
		for (Integer integer : list) {
			tree.add(integer);
		}
		
		System.out.println(tree.toString());
		
	}
	
	public List<Integer> place(int[] array){
		if(array.length == 0){
			return null;
		}
		Arrays.sort(array);
		return place(array,new ArrayList<Integer>(),0,array.length-1);
	}
	
	
	private List<Integer> place(int[] array,List<Integer> list, int from, int to) {
		int idx = (from+to)/2;
		list.add(array[idx]);
		if(idx != from){
			place(array,list, from, idx-1);
		}
		
		if(idx != to){
			place(array,list, idx+1, to);
		}
		return list;
	}

	void place(int[] array, BinarySearchTree<Integer> tree, int from, int to){
		int idx = (from+to)/2;
		tree.add(array[idx]);
		if(idx != from){
			place(array,tree, from, idx-1);
		}
		
		if(idx != to){
			place(array,tree, idx+1, to);
		}
	}
	
	void place(int[] array, BinarySearchTree<Integer> t){
		if(array.length == 0){
			return;
		}
		Arrays.sort(array);
		place(array,t,0,array.length-1);
	}
	
	public static void main(String[] args) {
		new Task4_2();
	}
}

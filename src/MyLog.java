import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

public class MyLog implements Logger {

	private LinkedList<Integer> fifo = new LinkedList<>();
	private Map<Integer, Data> map = new HashMap<>();

	void startLog(int id, long time) {
		Data data = new Data();
		data.id = id;
		data.startTime = time;
		fifo.addLast(data.id);
		map.put(id, data);
	}

	void endLog(int id, long time) {
		map.get(id).endTime = time;
		
		while (!map.isEmpty() && map.get(fifo.peek()).hasEnded()) {
			int endedId = fifo.poll();
			Data data = map.get(endedId);
			map.remove(endedId);
			print(data);
		}
	}
	

	void print(Data data) {
		System.out.println("ID: " + data.id + ", " + data.startTime + ", "
				+ data.endTime);
	}

	class Data {
		int id;
		long startTime;
		long endTime = -1;
		
		boolean hasEnded(){
			return endTime != -1;
		}
		
	}

	public static void main(String[] args) {
		MyLog log = new MyLog();
		log.startLog(1, 7363);
		log.startLog(2, 7393);
		log.startLog(3, 7400);
		log.startLog(4, 7401);
		log.endLog(2, 7600);
		log.endLog(1, 7700);
		log.endLog(4, 7800);
		log.endLog(3, 7801);

	}
}
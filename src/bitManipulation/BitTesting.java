package bitManipulation;

public class BitTesting {
	public static void main(String[] args) {
		double d = 0.5;
		
		double sum = 0;
		
		for (int i = 1; i < 54; i++) {
			sum += d;
			d = d / 2;
		}
		System.out.println(sum);
		
		
		int i = (1 << 4) - 1;
		System.out.println(Integer.toBinaryString(i));
	}
}

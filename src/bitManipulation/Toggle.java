package bitManipulation;

public class Toggle {
	public static int toggle(int num, int pos) {
		if((num & (1 << pos)) == 0){
			num = num | 1 << pos;
		} else {
			num = num &  ~(1 << pos);
		}
		return num;
	}
	
	public static void p(int num){
		System.out.println(String.format("%8s", Integer.toBinaryString(num)).replace(' ', '0'));
	}
	
	public static void main(String[] args) {
		int  i = 8;
		i = toggle(i,3);
		i = toggle(i,3);
		i = toggle(i,2);
		i = toggle(i,1);
//		i = toggle(i,31);

		p(i);
	}
}

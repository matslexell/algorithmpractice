package bitManipulation;

import java.util.ArrayList;
import java.util.List;

public class Task5_3_FlipBitToWin {

	static int flipBit(int n) {
		if (n == -1) {
			return 32;
		}
		
		int prevValue = n < 0 ? 1 : 0;
		int prevConsecutiveOnes = 0;
		int preConsecutiveZero = 0;
		int count = n < 0 ? 1 : 0;
		int bestResult = 1;
		
		while(n != 0)  {
			n = n << 1;
			if (n < 0) { // Leading 1
				if (prevValue == 0) {
					preConsecutiveZero = count;
					count = 0;
					prevValue = 1;
					
				}
				count++;
			} else { // Leading 0
				if (prevValue == 1) {
					if(preConsecutiveZero == 1){
						bestResult = Math.max(bestResult, prevConsecutiveOnes+count+1);
					}else{
						bestResult = Math.max(bestResult, count+1);
					}
					prevConsecutiveOnes = count;
					count = 0;
					prevValue = 0;
				}
				count++;
			}
		} 

		return bestResult;
	}

	public static void main(String[] args) {
		System.out.println(flipBit(Integer.parseInt("11011101111", 2)));
		System.out.println(flipBit(Integer.parseInt("1101111001111101111", 2)));
		System.out.println(flipBit(Integer.parseInt("110111001111", 2)));
		System.out.println(flipBit(Integer.parseInt("001110111011111000", 2)));

		
		System.out.println(Integer.parseInt("11011101111", 2));
	}
}

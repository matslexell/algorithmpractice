package bitManipulation;

public class Task5_6_Conversion {
	static int flips(int a, int b){
		int n = a ^ b;
		a = 0;
		while(n != 0){
			if((n & 1) == 1){
				a++;
			}
			n = n >>> 1;
		}
		return a;
	}
	
public static void main(String[] args) {
	System.out.println(flips(Integer.parseInt("11101",2),Integer.parseInt("01111",2)));
}
}

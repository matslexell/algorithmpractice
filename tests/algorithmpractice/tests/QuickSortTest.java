package algorithmpractice.tests;

import static org.junit.Assert.*;
import static utils.Utilities.*;

import org.junit.Test;

import utils.Utilities;
import algorithmpractice.QuickSort;

public class QuickSortTest {

	@Test
	public void testQuickSort() {
		
		int[] sorted = QuickSort.sort(new int[] {1});
		assertArrayEquals(new int[]{1}, sorted);
		
		sorted = QuickSort.sort(new int[] {1,1});
		assertArrayEquals(new int[]{1,1}, sorted);
		
		sorted = QuickSort.sort(new int[] {1,2});
		assertArrayEquals(new int[]{1,2}, sorted);
		
		sorted = QuickSort.sort(new int[] {2,1});
		assertArrayEquals(new int[]{1,2}, sorted);
		
		sorted = QuickSort.sort(new int[] {-3,8,1,4,0,8,9,9,2,1});
		assertArrayEquals(new int[]{-3,0,1,1,2,4,8,8,9,9 }, sorted);
		
		sorted = QuickSort.sort(new int[] {5,5,5,5});
		assertArrayEquals(new int[]{5,5,5,5}, sorted);
		
		sorted = QuickSort.sort(new int[] {6,5,5,5,5});
		assertArrayEquals(new int[]{5,5,5,5,6}, sorted);
	}

}
